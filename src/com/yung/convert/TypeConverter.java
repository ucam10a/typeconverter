package com.yung.convert;

public class TypeConverter {

    @SuppressWarnings("unchecked")
    public static <T> T convert(Object obj, String type){
        return (T) convert(obj, getType(type));
    }
    
    private static <T> T convert(Object from, Class<T> to) {
        if (to.isAssignableFrom(from.getClass())) {
            return to.cast(from);
        } else {
            throw new RuntimeException("No type");
        }
    }
    
    @SuppressWarnings("rawtypes")
    private static Class getType(String type) {
        if (type.equals("test.Person")){
            return Person.class;
        } else {
            return null;
        }
    }
    
    public static void main(String[] args) {
        
        Teacher t = new Teacher();
        t.setAddress("123");
        
        Person p = TypeConverter.convert(t, "test.Person");
        System.out.print(p.getAddress());
        
    }
    
}
